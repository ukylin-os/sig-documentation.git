
# <center>使用KVM虚拟机</center>
#### <center>作者：ymz316</center>
#### <center>2022-05-12 17:50:00</center>
<br>

KVM（Kernel-based Virtual Machine）是基于内核(内核内建)的虚拟机。它一般由QEMU来配合完成虚拟机的创建，虚拟机镜像的后缀名大多为qcow2。

KVM虚拟机安装前须知：

>1. KVM本身未模拟任何硬件设备，它使用的是硬件提供的虚拟化能力，比如(Intel VT-x, AMD-V, ARM virtualization extensions等)，因此KVM **需要芯片支持并开启虚拟化技术**（英特尔的 VT 扩展或者 AMD 的 AMD-V 扩展）。
>2. KVM需要由QEMU来配合完成主板、内存及I/O等硬件设备模拟，而且还**需要安装一些管理软件包**来达到快速化创建虚拟机的目的。
>3. 虚拟机安装需要使用网络，因此需要先**配置网络信息**，为虚拟机安装提供网络设备支持。
>4. 以上内容准备好之后，就可以**创建虚拟机**了。 

### 一、安装KVM的先决条件

**1) 确认是否支持硬件虚拟化（英特尔的 VT 扩展或者 AMD 的 AMD-V 扩展）**

```
$ LC_ALL=C lscpu | grep Virtualization
Virtualization:                  VT-x
```
或者：
```
$ grep -Eoc '(vmx|svm)' /proc/cpuinfo   #或者 egrep -c '(vmx|svm)' /proc/cpuinfo
16
```

如果输出一个大于零的数字(即CPU核心数)，表示CPU支持硬件虚拟化；输出0，表示CPU不支持硬件虚拟化,尝试重启进入BIOS设置中启用VT技术。

注：此处的$是终端提示符，不需要复制

**2)确定服务器是否能够运行硬件加速的KVM虚拟机**

```
$ kvm-ok   #如果提示无此命令，通过sudo apt install cpu-checker 来安装
INFO: /dev/kvm exists
KVM acceleration can be used
```

注：此处的$是终端提示符，不需要复制

### 二、安装KVM虚拟化管理软件包

**KVM 有关的虚拟化管理软件包**

>**qemu-kvm** ：为KVM管理程序提供硬件仿真的软件。20.04需要安装此包，22.04好像集成了，不需要安装，待查。  
**libvirt**：管理虚拟机和其他虚拟化功能(比如存储管理，网络管理)的软件集合。它包括一个API库，一个守护程序（libvirtd）和一个命令行工具（virsh）。它为受支持的虚拟机监控程序实现的常用功能提供通用的API。libvirt的主要目标是为各种虚拟化工具提供一套统一可靠的API，让上层可以用一种单一的方式来管理多种不同的虚拟化技术,它可以操作包括 KVM，vmware，XEN，Hyper-v, LXC 等 Hypervisor。需要通过安装**libvirt-daemon-system**包来将libvirt守护程序作为系统服务运行的配置文件。  
**libvirt-clients** ：用于管理虚拟化平台的软件,一般情况下，在安装libvirt-daemon-system时会自动安装此包。  
**virt-manager** ：基于 libvirt 的 GUI 工具 (图形用户界面)。  
**virtinst** ：一组用于创建虚拟机的命令行工具，一般情况下，在安装virt-manager时会自动安装此包。  
**bridge-utils** ：用于配置以太网桥的命令行工具。  
**ovmf**:虚拟机的UEFI 固件，安装后方可是虚拟机系统从uefi启动。

安装命令为：

```
sudo apt install qemu qemu-kvm libvirt-daemon-system virt-manager bridge-utils ovmf
```

### 三、客户机网络配置

客户机安装完成后，需要为其设置网络接口，以便和主机网络，客户机之间的网络通信。而linux系统安装时就需要使用网络通信，因此得提前设置客户机的网络连接。

KVM 客户机网络连接有两种方式：

- **用户网络（User Networking）**：也就是NAT方式。让虚拟机访问主机、互联网或本地网络上的资源的简单方法，但是不能从网络或其他的客户机访问客户机，性能上也需要大的调整。
- **虚拟网桥（Virtual Bridge）**：也就是Bridge(桥接)方式。Bridge方式可以使虚拟机成为网络中具有独立IP的主机，因此客户机和子网里面的机器能够互相通信。这种方式要比用户网络复杂一些，但是设置好后客户机与互联网，客户机与主机之间的通信都很容易。

以NAT为例进行学习：

终端输入`sudo virt-manager`或者在开始菜单点击"虚拟系统管理器",打开virt-manager图形界面。

![](../assets/KVM/virtmanager.png)

点击"编辑"-"连接详情"，进入网络配置界面，并选择虚拟网络，这里默认有一个default的NAT网络连接，设备名为virbr0，网段为192.168.122.0/24。

![](../assets/KVM/net.png)


可以点击下方的加号来创建一个新的网络连接，这里我们创建一个名为network的网络连接,forward to 选择"任意物理设备"(将创建一个网卡设备virbr1),网段为192.168.100.0/24，设置好DHCP，点击完成即可配置成功。下一步在虚拟机安装过程中可以设置为这个网络连接方式。

![](../assets/KVM/addnet.png)


### 四、客户虚拟机安装

虚拟机安装可以通过virt-manager图形化工具安装，也可通过qemu-img命令来安装。

**1. virt-manager图形化工具安装**

终端输入`sudo virt-manager`或者在开始菜单点击"虚拟系统管理器",打开virt-manager图形界面。

![](../assets/KVM/virtmanager.png)


1) 点击"文件"-"创建虚拟机" ，或者点击创建虚拟机的图标，然后选择本地安装介质，点击前进。

>注意：如果我们已经在网上或者别处拷贝了一个已经安装好系统的虚拟机磁盘镜像，则可以选择"导入现有磁盘映像"。

![](../assets/KVM/addstep1.png)

2) 选择安装源(ISO)和虚拟机系统格式(如red hat Linux 8之类的)，点击前进

![](../assets/KVM/addstep2.png)

3) 输入虚拟机内存，cpu核心数(不能大于主机核心数)，点击前进

![](../assets/KVM/addstep3.png)

4) 选择磁盘镜像(虚拟机安装也要有虚拟的磁盘吧)，这里我们创建一个40G的磁盘，点击前进

> 注意：这里默认的磁盘镜像路径是/var/lib/libirt/images/*.qcow2, 而如果我们需要使用一个已经创建好的磁盘镜像或者需要自定义磁盘镜像的存储位置，则可以选择“选择或创建自定义存储”

![](../assets/KVM/addstep4.png)

5) 输入虚拟机实例名称，选择网络，点击前进。

> 如果不懂如何选择网络，可能要补习一下虚拟机网络的几种模式(NAT,网桥，only_host)的知识了。

![](../assets/KVM/addstep5.png)

6) 此时，就进入了虚拟机系统的安装环节了

![](../assets/KVM/ossetup.png)

若提示“连接到图形控制台出错:Error opening Spice console, SpiceClientGtk missing”的错误，可试着通过如下命令解决：

```
hollowman@hollowman-F117:~$ apt search SpiceClientGtk
正在排序... 完成
全文搜索... 完成  
gir1.2-spiceclientgtk-3.0/focal 0.37-2fakesync1 amd64
  GTK3 widget for SPICE clients (GObject-Introspection)

hollowman@hollowman-F117:~$ sudo apt install gir1.2-spiceclientgtk-3.0 
```
**2. 命令安装**

**1) 创建虚拟机磁盘镜像**

```
$ qemu-img create -f qcow2 diskname.qcow2 40G
$ ll -h diskname.qcow2   #查看镜像在主机系统上的大小，虽然有40G磁盘，但因为没有数据，实际只占用196K
-rwxrwxrwx 1 hollowman hollowman 196K 11月 24 13:30 diskname.qcow2*
```

**2) 安装虚拟机(virt-install命令)**

a. 从ISO镜像中创建虚拟机：

```
# virt-install --name 客户机实例名 --memory 内存大小 --vcpus cpu核心数 --disk path=磁盘镜像路径 --cdrom=安装介质路径 --network 网络接口 --cdrom=安装介质路径 
#例如：
$ virt-install --name rhel8 --memory 2048 --vcpus 2 --disk path=./rhel8.qcow2 --cdrom=/media/hollowman/软件/ISO/rhel-8.0-x86_64-linuxprobe.com.iso --network bridge=virbr1
```

b. 用已有的qcow磁盘镜像创建虚拟机

```
# virt-install --name 客户机实例名 --memory 内存大小 --vcpus cpu核心数 --disk path=磁盘镜像路径 --network 网络接口 --import
# 例如：
$sudo virt-install --name openEuler2109 --memory 2048 --vcpus 2 --disk path=./openEuler-21.09-x86_64.qcow2 --network bridge=virbr1 --import
```

virt-install有关选项说明可通过命令查看：

```
$ virt-install help #下面为部分摘抄

通用选项:
  -n NAME, --name NAME             客户机实例名称
  --memory MEMORY                  客户机内存大小. 例如:--memory 1024 (in MiB)
  --vcpus VCPUS                    客户机cpu核心数(不大于主机核心数). 如:--vcpus 5

安装方法选项:
  --cdrom CDROM                    光驱安装介质
  --import                         在已有的磁盘镜像中构建客户机

OS options:
  --os-variant OS_VARIANT          指定操作系统类型。例如：rhl8.0 ，可通过'osinfo-query'命令来查看所有操作系统类型

设备选项:
  --disk DISK                      指定存储的各种选项。例如：--disk size=10 (在默认位置创建 10GiB 镜像)
  -w NETWORK, --network NETWORK    配置客户机网络接口。例如：--network bridge=mybr0

```

### 五、虚拟机管理过程发现的问题处理

> 1.qcow2镜像占用空间过大

如果通过virt-manager图形化界面来创建一个40G的磁盘镜像时，发现在主机上的存储空间却是40G，这么大的占用空间，是不是有强迫症？我们可以通过命令压缩来进行处理。

如下，我们查看virt-manager创建的磁盘镜像信息：

```
$ qemu-img info rhel8.qcow2   #查看磁盘镜像信息
image: rhel8.qcow2
file format: qcow2
virtual size: 40 GiB (42949672960 bytes)     #虚拟磁盘大小
disk size: 196 KiB                           #此为镜像实际占用空间
cluster_size: 65536
Format specific information:
    compat: 1.1
    lazy refcounts: false
    refcount bits: 16
    corrupt: false

$ ll -h rhel8.qcow2   #查看镜像在主机系统上的存储空间(这是通过virt-manager创建的磁盘)
-rwxrwxrwx 1 hollowman hollowman 40G 11月 24 13:48 rhel8.qcow2*

```

输入压缩命令：

```
$ qemu-img convert -O qcow2 rhel8.qcow2 rhel8_new.qcow2
$ ll -h rhel8_new.qcow2
-rwxrwxrwx 1 hollowman hollowman 196K 11月 24 13:52 rhel8_new.qcow2*
```
可以发现，存储空间变回196K了。

> 2.主机系统挂载qcow2磁盘镜像

qcow2本质上是一个虚拟的磁盘镜像，因此可在主机系统中通过挂载磁盘镜像分区的方式来读取该磁盘镜像分区的内容(如果是虚拟机磁盘，则需要在关闭虚拟机的情况下操作)。


> 3. 创建虚拟机时，出现'libvirt-qemu' 用户没有搜索目相关录权限的提示，创建不成功。

查看并修改配置文件`/etc/libvirt/qemu.conf`,找到 `# user = "root"`这一行并将注释去掉，重启libvirtd服务

```
$ sudo pluma /etc/libvirt/qemu.conf     #修改下面这一行
user = "root"

$ systemctl restart libvirtd

```
> 4.打开虚拟机时，出现“连接到图形控制台出错：Error opening SPICE console: Namespace SpiceClientGtk not available”

```
sudo apt install spice-client-gtk  gir1.2-spiceclientgtk-3.0
```

