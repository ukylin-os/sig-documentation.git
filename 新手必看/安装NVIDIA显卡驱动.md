# <center>安装NVIDIA显卡驱动</center>
#### <center>作者：陌生人</center>
#### <center>2022-05-27 22:35:54</center>

目前来说，在优麒麟安装英伟达的显卡驱动是非常简单的，主要是因为优麒麟的上游Ubuntu把英伟达的闭源驱动集成了，大家再也不用像以前那样去英伟达的官网下载二进制驱动然后关闭x window安装了，让新人也可以非常快速简单的安装好自己对应的显卡驱动。

再次稍稍解释下为啥要装显卡驱动，显卡驱动装好了以后很多的软件可以调用显卡加速来降低CPU的使用率从而降低一定的功耗，比如视频硬解码，视频转码，浏览器的GPU硬件加速，如果没有驱动的话这些相当耗费性能的计算都会给CPU去完成，结果就是CPU使用率暴增。好了，下面进入正题。
首先打开软件更新器，从开始菜单里就可以找到

![图片1.png](../assets/%E5%AE%89%E8%A3%85NVIDIA%E6%98%BE%E5%8D%A1%E9%A9%B1%E5%8A%A8/%E5%9B%BE%E7%89%871.png)

打开以后软件更新器会自动更新软件信息，注意，此处只是更新软件信息，并不会升级

![图片2.png](../assets/%E5%AE%89%E8%A3%85NVIDIA%E6%98%BE%E5%8D%A1%E9%A9%B1%E5%8A%A8/%E5%9B%BE%E7%89%872.png)

此处视网络速度和硬件性能影响，会有不同的等待时间，总的来说只要等他检查结束即可，建议这一步不要跳过，有可能会无法下载驱动

检查完成后会提示当前可以升级软件包，可以升级也可以选择延后，一般建议大家升级更新，这个时候点击左下角的设置

![图片3.png](../assets/%E5%AE%89%E8%A3%85NVIDIA%E6%98%BE%E5%8D%A1%E9%A9%B1%E5%8A%A8/%E5%9B%BE%E7%89%873.png)

点击以后会有一个新的弹窗，可能需要等待几秒这个弹窗才会出来，然后点击**附加驱动**

![图片4.png](../assets/%E5%AE%89%E8%A3%85NVIDIA%E6%98%BE%E5%8D%A1%E9%A9%B1%E5%8A%A8/%E5%9B%BE%E7%89%874.png)

此时会显示正在搜索的字样，稍微等一下就会出现一些驱动，一般来说都是显卡的，也有一些是别的设备的，如果没有安装显卡驱动的话在列表最下方的**使用X.Org X server**是被选中的，这个是内核里的开源驱动，一般情况下是可以让显卡正常显示，但是像是硬件加速这些功能一般是没办法用的，这个时候我们可以从列表里挑选一个驱动，一般最上边的是比较推荐的，只要选中最上边的那个驱动，然后点击右下角的应用更改就可以了

![图片5.png](../assets/%E5%AE%89%E8%A3%85NVIDIA%E6%98%BE%E5%8D%A1%E9%A9%B1%E5%8A%A8/%E5%9B%BE%E7%89%875.png)

如果点错了还可以点击应用更改旁边的还原，应用更改以后前边会有一个小的滚动条，滚动条走完以后就可以重启电脑来使用对应的闭源驱动了。